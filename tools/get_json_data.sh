#!/bin/bash

#
# helping debug stuff
#

#MATCH_OLD_TEST_FILES="^ok: \[localhost\] => \(item=old_test_files\.files\|map\(attribute='path'\)\|list\) => \{\s*$"
MATCH_ANY_JSON_DATA="^ok: \[localhost\] => (\(.+\) => )?\{\s*$"

#
# stdin -> source data
# $1    -> starting match
function get_json_data {
  if [ ! -z $1 ]; then
    MATCH_REGEX="$1";
  else
    MATCH_REGEX="$MATCH_ANY_JSON_DATA"
  fi
  echo "["
  sed -n -r -e "/$MATCH_REGEX/,/^\s*$/p" | sed -r -e "s/$MATCH_REGEX/{/" | sed -r -e '/^\s*\}\s*$/,/^\s*\{\s*$/s/^\s*$/,/' | head -n -1
  echo "]"
}

# remove color/control sequences from the stdin data
function strip_control_sequences {
   sed -r "s/\x1B\[([0-9]{1,3}(;[0-9]{1,2})?)?[mGK]//g"
}
