#!/usr/bin/python

from ansible.utils.display import Display
from jinja2.utils import is_undefined
import re

class FilterModule(object):
    
    def filters(self):
        return {
            'dict_default': self.dict_default,
            'dict_deep_combine': self.dict_deep_combine,
            'expand_port_ranges': self.expand_port_ranges,
            'reconcile_port_settings': self.reconcile_port_settings,
            'combine_settings': self.combine_settings,
            'extract_hosts': self.extract_hosts,
            'merge_hosts': self.merge_hosts,
            'resolve_cnames': self.resolve_cnames
        }

    
    # make sure all keys from default exist in arg
    # if arg is not a dict, create the keys but set them to arg's original value
    def dict_default(self, arg, default={}):

        # default needs to be a dict
        if not isinstance(default, dict):
            raise TypeError("Expected a dict: default (%s)" % type(default))
        
        # if arg is underfined, go with default
        if (is_undefined(arg)):
            return default
          
        # if arg is not a dict, return a dict with keys from default
        # but values set to arg
        if not isinstance(arg, dict):
            return {default_index:arg for default_index in default}
        
        # arg is a dict, default is a dict,
        # make sure all keys from default exist in arg
        for key in default:
            if not key in arg:
                arg[key] = default[key]
        return arg


    # combine source and overlay dicts such that:
    # - all keys from overlay are included
    # - if values of a given key is a list or a dict in both source and overlay,
    #   merge the lists
    # - if values of a given key are of different types in source and overlay,
    #   overwrite source[key] with overlay[key]
    def dict_deep_combine(self, source, overlay):
        
        # reality checks
        if not isinstance(source, dict):
            raise TypeError("Expected a dict: source (%s)" % type(source))
        if not isinstance(overlay, dict):
            raise TypeError("Expected a dict: overlay (%s)" % type(overlay))
        
        # copy the source to the result
        result = {**source}
        
        for k in overlay:
            
            if k in source:
                # if k is in source:
                # - unique list merge if both source[k] and overlay[k] are lists
                # - shallow dict merge if both source[k] and overlay[k] are dicts
                # - use overlay[k] in any other case (including if types differ)
                if isinstance(source[k], list) and isinstance(overlay[k], list):
                    result[k] = list(set([*source[k], *overlay[k]]))
                # for dicts we need to be sneaky and go deep
                elif isinstance(source[k], dict) and isinstance(overlay[k], dict):
                    result[k] = self.dict_deep_combine(source[k], overlay[k])
                # everything else is simple
                else:
                    result[k] = overlay[k]
                    
            else:
                # k is not in source, just use overlay[k]
                result[k] = overlay[k]
                
            # sort stuff that is sortable
            if isinstance(result[k], list):
                result[k].sort()
            elif isinstance(result[k], dict):
                unsorted=result[k]
                result[k]={}
                for j in sorted(unsorted.keys()):
                    result[k][j]=unsorted[j]
                
        return result
    

    # expand port ranges in a set of settings
    # we need to do this to all user-defined sets of settings,
    # otherwise we cannot reason about individual ports
    def expand_port_ranges(self, settings):
        
        # if we have nothing to do, we're done
        if 'ports' not in settings.keys():
            return settings
        
        # this is a skip
        if settings['ports'] == 'skip':
            return settings
        
        for proto in settings['ports'].keys():
            
            # skip
            if settings['ports'][proto] == 'skip':
                continue
            
            if proto in ['tcp', 'udp']:
                # list-based portlists ('tcp', 'udp')
                clean_portlist = []
                for p in settings['ports'][proto]:
                    # nice, clean integer? just add it to the list
                    if isinstance(p, int):
                        clean_portlist += [p]
                    elif isinstance(p, str):
                        try:
                            # a string containing an integer? just add it to the list
                            clean_portlist += [int(p)]
                        except ValueError:
                            try:
                                # ah, perhaps a range?
                                pstart, pend = [int(x) for x in p.split('-')]
                                if pstart > pend:
                                    Display().warning("Ignoring empty range: %s" % p)
                                else:
                                    clean_portlist += range(pstart, pend+1)
                            except ValueError:
                                raise TypeError("Not a valid port nor port range definition: '%s'" % p)
                            
                    else:
                        raise TypeError('Expected int or str, got: %s' % type(p))
                    
                # assign the cleaned-up list
                settings['ports'][proto]=sorted(set(clean_portlist))
                
            elif proto in ['tls']:
                # dict-based portlists
                clean_portlist = {}
                for p, v in settings['ports'][proto].items():
                    # nice, clean integer? just add it to the list
                    if isinstance(p, int):
                        clean_portlist[p] = v
                    elif isinstance(p, str):
                        try:
                            # a string containing an integer? just add it to the list
                            clean_portlist[int(p)] = v
                        except ValueError:
                            try:
                                # ah, perhaps a range?
                                pstart, pend = [int(x) for x in p.split('-')]
                                if pstart > pend:
                                    Display().warning("Ignoring empty range: %s" % p)
                                else:
                                    for pnew in range(pstart, pend+1):
                                        clean_portlist[pnew] = v
                            except ValueError:
                                raise TypeError("Not a valid port nor port range definition: '%s'" % p)
                            
                    else:
                        raise TypeError('Expected int or str, got: %s' % type(p))
                    
                # assign the cleaned-up list
                settings['ports'][proto]=clean_portlist
                
            else:
                raise TypeError('Unknown proto: %s' % proto)
        
        return settings


    # filters out TLS settings from the "tls" key
    # based on which TCP ports are configured as allow-open in the "tcp" key
    # this is to be used *after* all relevant settings are combined/merged
    def reconcile_port_settings(self, ports):
        
        # these are skips
        if not isinstance(ports['tcp'], list) or not isinstance(ports['tls'], dict):
            return ports
        
        tlsports = {}
        for p in ports['tls']:
            if p in ports['tcp']:
                tlsports[p] = ports['tls'][p]
                
        ports['tls'] = tlsports
        return ports

    
    # combine prober settings,
    # making sure structure of the 'ports' key is correct in the end
    def combine_settings(self, source, overlay, default_ports={'tcp': [], 'udp': [], 'tls':{}}):
        
        # first, make sure 'ports' key even exists in the source
        # and has the correct structure; use the default instead if not
        if 'ports' in source:
            source['ports'] = self.dict_default(source['ports'], default_ports)
        else:
            source['ports'] = default_ports
        
        # combine source with overlay
        result = self.dict_deep_combine(source, overlay)
        
        # finally, make sure 'ports' key in result also has the correct structure
        # no need to check if it exists at this stage
        result['ports'] = self.dict_default(result['ports'], default_ports)
            
        return result
    
    
    # extracting hosts from zone data
    # 
    # source is a string or an array of strings
    # domain is a string
    # returns an array of strings ("<IP> <hostname>")
    def extract_hosts(self, source, domain):
        
        # we need a list of strings, or a string
        if (isinstance(source, str)):
            data = source.split('\n')
        elif (isinstance(source, list)):
            data = source.copy()
        else:
            raise TypeError('Expected a list or a string')
        
        cnames = []
        addresses = []
        for l in data:
            # clean up
            l = ' '.join(l.strip().replace('\t', ' ').split())
            
            # ignore comments and localhost
            if l == '' or l[0] == ';' or l.find('localhost') >= 0:
                continue
            
            # we want A/AAAA records here
            if l.find(' IN A ') >= 0 or l.find(' IN AAAA ') >= 0:
                
                # just the IP -> hostname mapping
                hostname, ip = re.sub(r'^(.+) [0-9]+ IN A+ (.+)$', r'\1 \2', l).split(' ')
                
                # if we're dealing with an `@` record, replace it with the domain
                if hostname == '@':
                    hostname = domain
                # if it's ending with a dot, we're good; just remove the dot
                elif hostname[-1:] == '.':
                    hostname = hostname[:-1]
                # if there is no dot, we need to add the domain
                else:
                    hostname += '.' + domain
                
                # add to the addresses lost
                addresses.append([ip, hostname])
                
                # next please
                continue
            
            # here, we want CNAME records
            if l.find(' IN CNAME ') >= 0:
                
                # just the name -> cname mapping
                name, cname = re.sub(r'^(.+) [0-9]+ IN CNAME (.+)$', r'\1 \2', l).split(' ')
                
                # if it ending in a dot, we're good, just remove the dot
                if cname[-1:] == '.':
                    cname = cname[:-1]
                # if there is no dot, we need to add the domain
                else:
                    cname += '.' + domain
                
                # if it ending in a dot, we're good, just remove the dot
                if name[-1:] == '.':
                    name = name[:-1]
                # if there is no dot, we need to add the domain
                else:
                    name += '.' + domain
                
                # add to the addresses lost
                cnames.append([name, cname])
        
        return {
            "cnames": sorted(cnames),
            "addresses": sorted(addresses)
        }
    
    
    # merging the host list by IP
    # 
    # source is a string or a list of lists of strings; each line (or element) is of the form: "<IP> <hostname>" or ["<IP>", "<hostname>"]
    # returns a list of lists of strings (["<IP>", "<hostname1>", "<hostname2>", "<...>"])
    def merge_hosts(self, source):
        
        # we need a list of strings, or a string
        if (isinstance(source, str)):
            data = source.split('\n')
        elif (isinstance(source, list)):
            data = source.copy()
        else:
            raise TypeError('Expected a list or a string')
        
        result = {}
        for l in data:
            # clean up and split, if string
            if (isinstance(l, str)):
                ip, hostname = l.strip().replace('\t', ' ').split()
            # if already a list, just unpack please
            else:
                ip, hostname = l
                
            # add to the result dict
            if ip in result:
                result[ip].append(hostname)
            else:
                result[ip]=[hostname]
            
        # we want a list of strings, after all
        # and while we're at it, let's make sure the hostnames do not repeat
        return sorted([[ip, *sorted(set(result[ip]))] for ip in result])


    # resolving cnames based on [IP, hostname] list
    # 
    # cnames is a a list of lists of strings; each elementis of the form: ["<name>", "<cname>"]
    # addresses is a a list of lists of strings; each elementis of the form: ["<IP>", "<hostname>"]
    # returns a list of lists of strings (["<IP>", "<name>"]) whenever <cname> == <hostname>
    def resolve_cnames(self, cnames, addresses):
        
        # some rules for CNAMEs
        # 1. can't have multiple CNAME records for a single name
        # 2. CNAME chains are allowed but discouraged
        # 3. CNAME loops are possible but definitely something we should flag
        # ...and we need to discover and report these if we want to be robust
        # 
        # obviously they should also be handled in the tests themselves
        
        # first, make sure there are no repeated items
        cnames = [list(t) for t in list(set(tuple(c) for c in cnames))]
        
        # saving problematic cnames, with a reason
        problematic_cnames = {
            "ambiguous": [],
            "looped": [],
            "unresolvable": []
        }
        
        # second, make sure a single name does not have multiple cnames
        csize = len(cnames)
        i = 0
        for i in range(0, csize):
            # if we already know it's ambiguous, no need to go through the motions
            if cnames[i][0] not in problematic_cnames['ambiguous']:
                for j in range(i + 1, csize):
                    if cnames[i][0] == cnames[j][0]:
                        Display().warning("Ambiguous CNAMEs for: %s" % cnames[i][0])
                        problematic_cnames['ambiguous'].append(cnames[i][0])
                        break
        
        #print(cnames)
        
        # resolve any CNAME chains (finding any loops along the way)
        # 
        # at this point we know that every name->cname tuple only shows up once
        # 
        # yes, this code is an abomination and one day needs to be rewritten
        found_chain = True
        while found_chain:
            found_chain = False
            clen = len(cnames)
            i = 0
            #print('clen: %2d' % clen)
            while i < clen:
                #print('clen: %2d; i: %2d; cnames[i]: %s' % (clen, i, cnames[i]))
                # do we have a loop?
                if cnames[i][0] == cnames[i][1]:
                    #print('clen: %2d; i: %2d; cnames[i]: %s; loop!' % (clen, i, cnames[i]))
                    # we absoloopely do! make a note of that 
                    Display().warning("Looped CNAME for: %s" % cnames[i][0])
                    problematic_cnames['looped'].append(cnames[i][0])
                    # and remove it from the set, since we can't really do anything with it
                    del cnames[i]
                    # update the count
                    clen -= 1
                    #print('clen: %2d; i: %2d; cnames: %s' % (clen, i, cnames))
                    # and go again
                    continue
                    #raise Exception('CNAME loop detected: from %s onto itself' % cnames[i][0])
                j = 0
                while j < clen:
                    #print('clen: %2d; i: %2d; cnames[i]: %s; j: %2d; cnames[j]: %s' % (clen, i, cnames[i], j, cnames[j]))
                    # is the current cname same as a name somewhere later?
                    while j != i and cnames[i][1] == cnames[j][0]:
                        #print('clen: %2d; i: %2d; cnames[i]: %s; j: %2d; cnames[j]: %s; match!' % (clen, i, cnames[i], j, cnames[j]))
                        # set the flag
                        found_chain = True
                        # loop?
                        if cnames[i][0] == cnames[j][1]:
                            #print('clen: %2d; i: %2d; cnames[i]: %s; j: %2d; cnames[j]: %s; loop!' % (clen, i, cnames[i], j, cnames[j]))
                            # yes. make a note of that 
                            Display().warning("Looped CNAME for: %s" % cnames[i][0])
                            problematic_cnames['looped'].append(cnames[i][0])
                            # and remove it from the set, since we can't really do anything with it
                            del cnames[i]
                            # if the other element is *before* i, decrease i and remove that element
                            if j < i:
                                del cnames[j]
                                i -= 1
                            # otherwise, since we removed the i-th element, everything moved down one notch
                            # so remove the (j-1)-th element
                            else:
                                del cnames[j-1]
                            # update the count
                            clen -= 2
                            #print('clen: %2d; i: %2d; j: %2d; cnames: %s' % (clen, i, j, cnames))
                            # if we removed the last element, break
                            if j >= clen or i >= clen:
                                break
                            # and go again
                            continue
                            #raise Exception('Loop detected for %s' % cnames[i][0])
                        # replace the cname here and now
                        Display().warning("CNAME chain found: %s -> %s -> %s" % (cnames[i][0], cnames[i][1], cnames[j][1]))
                        cnames[i][1] = cnames[j][1]
                    else:
                        # increment the indexes
                        j += 1
                i += 1
                
        #print(cnames)
        
        # get a list of all the canonical names from cnames list
        # as a set so that we only need to resolve each once
        # and start resolving
        # some workspace pls
        resolved = []
        for name, canonical in cnames:
            # let's try to find it in our addresses list
            for ip, hostname in addresses:
                if canonical == hostname:
                    # the order is reversed here!
                    # name->CNAME becomes IP->name
                    resolved.append([ip, name])
                    break
            else:
                # not there? that's unexpected
                # TODO: we should probably handle this differently
                Display().warning("CNAME unresolvable within configured zones: %s -> %s" % (name, canonical))
                problematic_cnames['unresolvable'].append(canonical)
                #raise Exception('Could not resolve %s using the available data (CNAME leading outside of the configured zones?)' % canonical)
                
        # return what we got
        return {
            "resolved": sorted(resolved),
            "ambiguous": sorted(problematic_cnames['ambiguous']),
            "looped": sorted(problematic_cnames['looped']),
            "unresolvable": sorted(problematic_cnames['unresolvable'])
        }
